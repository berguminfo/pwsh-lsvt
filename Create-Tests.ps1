try {
    Remove-Item -Recurse -Force tests | Out-Null
} catch {}
New-Item -ItemType Directory tests | Out-Null
Push-Location
try {
    Set-Location tests
    New-Item -ItemType Directory bin | Out-Null
    New-Item -ItemType Directory directory | Out-Null
    cmd.exe /c mklink /J reparse directory\ | Out-Null
    cmd.exe /c mklink /J root \ | Out-Null
    cmd.exe /c mklink /D symlink .\directory\ | Out-Null
    New-Item -ItemType Directory directory\child | Out-Null
    cmd.exe /c mklink /D symlink_child directory\child | Out-Null
    cmd.exe /c mklink /D symlink_symlink symlink | Out-Null
    New-Item -ItemType File .gitignore | Out-Null
    New-Item -ItemType File file | Out-Null
    New-Item -ItemType File hidden | Out-Null
    Set-ItemProperty hidden Attributes Hidden
    cmd.exe /c mklink symlink_broken broken | Out-Null
    cmd.exe /c mklink symlink_file text.txt | Out-Null
    New-Item -ItemType File text.cs | Out-Null
    New-Item -ItemType File text.txt | Out-Null
    cmd.exe /c mklink text_link.txt text.txt | Out-Null
    cmd.exe /c mklink text_link_absolute.txt C:\Windows\csup.txt | Out-Null
} catch {
    Write-Host "Error create Tests: $Error"
}
Pop-Location
ls tests