# pwsh-lsvt README

pwsh-lsvt - list directory contents.

![Screendump](screendump.png)

## Install

### Step 1
Install [PowerShell Core 6+](https://github.com/PowerShell/PowerShell).

### Step 2
Install and set as default font any NerdFont [https://www.nerdfonts.com/](https://www.nerdfonts.com/).

### Step 2
Open the `$profile` file for editing.
Import this module and optional set any alias.

```ps
Import-Module "$HOME\pwsh-lsvt\lsvt"
Set-Alias ls Get-ChildItemVt
ls
```
