# INSTALL
# -------
#
# Edit $PROFILE:
# + Import-Module "{path}\lsvt"
# + Set-Alias ls Get-ChildItemVt
#
# REMARKS
# -------
#
# In order to use SPACE parameters each item in the array has to be escaped:
# * ls 'C:\Program Files\'
# * ls "C:\Program Files\"
# * ls C:\Program` Files\
#

if (Get-Module lsvt) { return }

# See https://jonasjacek.github.io/colors/ for color names

$DodgerBlue1 = 33
$DarkTurquoise = 44
$Red3 = 124

$ItemCodes = @{
    Directory = $DodgerBlue1;
    Link = $DarkTurquoise;
    BrokenLink = $Red3
}

$Green3 = 40
$SpringGreen2 = 42
$DarkCyan = 36

$DateSelector = @{
#             <1 Hour                   <1 Day                   <Inf
    Terms = ( [TimeSpan]::FromHours(1), [TimeSpan]::FromDays(1), [TimeSpan]::MaxValue )
    Codes = ( $Green3,                  $SpringGreen2,           $DarkCyan )
}

$DarkViolet1 = 92
$DarkViolet2 = 128
$Magenta3 = 164

$LengthCodes = (
#   Single        Kilo-         Mega-         Giga-      Tera-      Peta-      Exa-
    $DarkViolet1, $DarkViolet1, $DarkViolet2, $Magenta3, $Magenta3, $Magenta3, $Magenta3
)

$NumberSelector = @{
    Terms = ( [double]::MaxValue )
    Codes = $LengthCodes
    Units = ( "B" )
    Format = "{0,20:N}"
}

$DecimalSelector = @{
    Terms = (
        [Math]::Pow(1000, 1), [Math]::Pow(1000, 2), [Math]::Pow(1000, 3),
        [Math]::Pow(1000, 4), [Math]::Pow(1000, 5), [Math]::Pow(1000, 6), [double]::MaxValue
    )
    Codes = $LengthCodes
    Units = ( "B", "kB", "MB", "GB", "TB", "PB", "EB" )
    Format = "{0,4:F0}"
}

$BinarySelector = @{
    Terms = (
        [Math]::Pow(1024, 1), [Math]::Pow(1024, 2), [Math]::Pow(1024, 3),
        [Math]::Pow(1024, 4), [Math]::Pow(1024, 5), [Math]::Pow(1024, 6), [double]::MaxValue
    )
    Codes = $LengthCodes
    Units = ( "B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB" )
    Format = "{0,5:N0}"
}

$IconsByName = @{
    ".Trash" = "`u{f1f8}" # ""
    ".atom" = "`u{e764}" # ""
    ".bashprofile" = "`u{e615}" # ""
    ".bashrc" = "`u{f489}" # ""
    ".git" = "`u{f1d3}" # ""
    ".gitattributes" = "`u{f1d3}" # ""
    ".gitconfig" = "`u{f1d3}" # ""
    ".github" = "`u{f408}" # ""
    ".gitignore" = "`u{f1d3}" # ""
    ".gitmodules" = "`u{f1d3}" # ""
    ".rvm" = "`u{e21e}" # ""
    ".vimrc" = "`u{e62b}" # ""
    ".vscode" = "`u{e70c}" # ""
    ".zshrc" = "`u{f489}" # ""
    "bin" = "`u{e5fc}" # ""
    "config" = "`u{e5fc}" # ""
    "docker-compose.yml" = "`u{f308}" # ""
    "dockerfile" = "`u{f308}" # ""
    "ds_store" = "`u{f179}" # ""
    "gitignore_global" = "`u{f1d3}" # ""
    "gradle" = "`u{e70e}" # ""
    "gruntfile.coffee" = "`u{e611}" # ""
    "gruntfile.js" = "`u{e611}" # ""
    "gruntfile.ls" = "`u{e611}" # ""
    "gulpfile.coffee" = "`u{e610}" # ""
    "gulpfile.js" = "`u{e610}" # ""
    "gulpfile.ls" = "`u{e610}" # ""
    "hidden" = "`u{f023}" # ""
    "include" = "`u{e5fc}" # ""
    "lib" = "`u{f121}" # ""
    "localized" = "`u{f179}" # ""
    "node_modules" = "`u{e718}" # ""
    "npmignore" = "`u{e71e}" # ""
    "rubydoc" = "`u{e73b}" # ""
    "yarn.lock" = "`u{e718}" # ""
    "Cargo.lock" = "`u{e7a8}" # ""
}

$IconsByExtension = @{
    ".apk" = "`u{e70e}" # ""
    ".avi" = "`u{f03d}" # ""
    ".avro" = "`u{e60b}" # ""
    ".awk" = "`u{f489}" # ""
    ".bash" = "`u{f489}" # ""
    ".bash_history" = "`u{f489}" # ""
    ".bash_profile" = "`u{f489}" # ""
    ".bashrc" = "`u{f489}" # ""
    ".bat" = "`u{f17a}" # ""
    ".bmp" = "`u{f1c5}" # ""
    ".c" = "`u{e61e}" # ""
    ".c++" = "`u{e61d}" # ""
    ".cc" = "`u{e61d}" # ""
    ".cfg" = "`u{e615}" # ""
    ".clj" = "`u{e768}" # ""
    ".cljs" = "`u{e76a}" # ""
    ".cls" = "`u{e600}" # ""
    ".coffee" = "`u{f0f4}" # ""
    ".conf" = "`u{e615}" # ""
    ".cp" = "`u{e61d}" # ""
    ".cpp" = "`u{e61d}" # ""
    ".csh" = "`u{f489}" # ""
    ".css" = "`u{e749}" # ""
    ".csv" = "`u{f1c3}" # ""
    ".cxx" = "`u{e61d}" # ""
    ".d" = "`u{e7af}" # ""
    ".dart" = "`u{e798}" # ""
    ".db" = "`u{f1c0}" # ""
    ".diff" = "`u{f440}" # ""
    ".doc" = "`u{f1c2}" # ""
    ".docx" = "`u{f1c2}" # ""
    ".ds_store" = "`u{f179}" # ""
    ".dump" = "`u{f1c0}" # ""
    ".ebook" = "`u{e28b}" # ""
    ".editorconfig" = "`u{e615}" # ""
    ".ejs" = "`u{e618}" # ""
    ".env" = "`u{f462}" # ""
    ".eot" = "`u{f031}" # ""
    ".epub" = "`u{e28a}" # ""
    ".erb" = "`u{e73b}" # ""
    ".erl" = "`u{e7b1}" # ""
    ".exe" = "`u{f17a}" # ""
    ".fish" = "`u{f489}" # ""
    ".flac" = "`u{f001}" # ""
    ".flv" = "`u{f03d}" # ""
    ".font" = "`u{f031}" # ""
    ".gdoc" = "`u{f1c2}" # ""
    ".gemfile" = "`u{e21e}" # ""
    ".gemspec" = "`u{e21e}" # ""
    ".gform" = "`u{f298}" # ""
    ".gif" = "`u{f1c5}" # ""
    ".git" = "`u{f1d3}" # ""
    ".go" = "`u{e626}" # ""
    ".gradle" = "`u{e70e}" # ""
    ".gsheet" = "`u{f1c3}" # ""
    ".gslides" = "`u{f1c4}" # ""
    ".guardfile" = "`u{e21e}" # ""
    ".gz" = "`u{f410}" # ""
    ".h" = "`u{f0fd}" # ""
    ".hbs" = "`u{e60f}" # ""
    ".hpp" = "`u{f0fd}" # ""
    ".hs" = "`u{e777}" # ""
    ".htm" = "`u{f13b}" # ""
    ".html" = "`u{f13b}" # ""
    ".hxx" = "`u{f0fd}" # ""
    ".ico" = "`u{f1c5}" # ""
    ".image" = "`u{f1c5}" # ""
    ".iml" = "`u{e7b5}" # ""
    ".ini" = "`u{f17a}" # ""
    ".ipynb" = "`u{e606}" # ""
    ".jar" = "`u{e204}" # ""
    ".java" = "`u{e204}" # ""
    ".jpeg" = "`u{f1c5}" # ""
    ".jpg" = "`u{f1c5}" # ""
    ".js" = "`u{e74e}" # ""
    ".json" = "`u{e60b}" # ""
    ".jsx" = "`u{e7ba}" # ""
    ".ksh" = "`u{f489}" # ""
    ".less" = "`u{e758}" # ""
    ".lhs" = "`u{e777}" # ""
    ".license" = "`u{f48a}" # ""
    ".localized" = "`u{f179}" # ""
    ".lock" = "`u{e21e}" # ""
    ".log" = "`u{f18d}" # ""
    ".lua" = "`u{e620}" # ""
    ".m4a" = "`u{f001}" # ""
    ".markdown" = "`u{f48a}" # ""
    ".md" = "`u{f48a}" # ""
    ".mkd" = "`u{f48a}" # ""
    ".mkv" = "`u{f03d}" # ""
    ".mobi" = "`u{e28b}" # ""
    ".mov" = "`u{f03d}" # ""
    ".mp3" = "`u{f001}" # ""
    ".mp4" = "`u{f03d}" # ""
    ".mustache" = "`u{e60f}" # ""
    ".npmignore" = "`u{e71e}" # ""
    ".ogg" = "`u{f001}" # ""
    ".ogv" = "`u{f03d}" # ""
    ".otf" = "`u{f031}" # ""
    ".pdf" = "`u{f1c1}" # ""
    ".php" = "`u{e73d}" # ""
    ".pl" = "`u{e769}" # ""
    ".png" = "`u{f1c5}" # ""
    ".ppt" = "`u{f1c4}" # ""
    ".pptx" = "`u{f1c4}" # ""
    ".procfile" = "`u{e21e}" # ""
    ".properties" = "`u{e60b}" # ""
    ".ps1" = "`u{f489}" # ""
    ".psd" = "`u{e7b8}" # ""
    ".pxm" = "`u{f1c5}" # ""
    ".py" = "`u{e606}" # ""
    ".pyc" = "`u{e606}" # ""
    ".r" = "`u{f25d}" # ""
    ".rakefile" = "`u{e21e}" # ""
    ".rar" = "`u{f410}" # ""
    ".rb" = "`u{e21e}" # ""
    ".rdata" = "`u{f25d}" # ""
    ".rdb" = "`u{e76d}" # ""
    ".rdoc" = "`u{f48a}" # ""
    ".rds" = "`u{f25d}" # ""
    ".readme" = "`u{f48a}" # ""
    ".rlib" = "`u{e7a8}" # ""
    ".rmd" = "`u{f48a}" # ""
    ".rs" = "`u{e7a8}" # ""
    ".rspec" = "`u{e21e}" # ""
    ".rspec_parallel" = "`u{e21e}" # ""
    ".rspec_status" = "`u{e21e}" # ""
    ".rss" = "`u{f09e}" # ""
    ".ru" = "`u{e21e}" # ""
    ".rubydoc" = "`u{e73b}" # ""
    ".sass" = "`u{e603}" # ""
    ".scala" = "`u{e737}" # ""
    ".scss" = "`u{e749}" # ""
    ".sh" = "`u{f489}" # ""
    ".shell" = "`u{f489}" # ""
    ".slim" = "`u{e73b}" # ""
    ".sql" = "`u{f1c0}" # ""
    ".sqlite3" = "`u{e7c4}" # ""
    ".styl" = "`u{e600}" # ""
    ".stylus" = "`u{e600}" # ""
    ".svg" = "`u{f1c5}" # ""
    ".swift" = "`u{e755}" # ""
    ".tar" = "`u{f410}" # ""
    ".tex" = "`u{e600}" # ""
    ".tiff" = "`u{f1c5}" # ""
    ".ts" = "`u{e628}" # ""
    ".tsx" = "`u{e7ba}" # ""
    ".ttf" = "`u{f031}" # ""
    ".twig" = "`u{e61c}" # ""
    ".txt" = "`u{f15c}" # ""
    ".video" = "`u{f03d}" # ""
    ".vim" = "`u{e62b}" # ""
    ".vue" = "`u{fd42}" # "﵂"
    ".wav" = "`u{f001}" # ""
    ".webm" = "`u{f03d}" # ""
    ".webp" = "`u{f1c5}" # ""
    ".windows" = "`u{f17a}" # ""
    ".woff" = "`u{f031}" # ""
    ".woff2" = "`u{f031}" # ""
    ".xls" = "`u{f1c3}" # ""
    ".xlsx" = "`u{f1c3}" # ""
    ".xml" = "`u{e619}" # ""
    ".xul" = "`u{e619}" # ""
    ".yaml" = "`u{f481}" # ""
    ".yml" = "`u{f481}" # ""
    ".zip" = "`u{f410}" # ""
    ".zsh" = "`u{f489}" # ""
    ".zsh-theme" = "`u{f489}" # ""
    ".zshrc" = "`u{f489}" # ""
}

$IconsByType = @{
    "FileInfo" = "`u{f016}" # ""
    "DirectoryInfo" = "`u{f115}" # ""
}

if ($Host.UI.SupportsVirtualTerminal) {
    function Fg { "`e[39m" }
    function Fg8($Color) { "`e[38;5;$($Color)m" }
} else {
    function Fg { "" }
    function Fg8($Color) { "" }
}

function Get-ChildItemVt {
    Param (

        #
        # Get-ChildItemVt Properties
        #

        [Alias("iec")]
        [switch] $BinaryLength = $false,

        [Alias("b")]
        [switch] $ByteLength = $false,

        [string] $DateStyle = "d",

        [string] $TimeStyle = "t",

        [Alias("l")]
        [switch] $ListMode = $true, # unused, simulates `ls -l`

        #
        # Get-ChildItem Properties
        #

        [Parameter(
            Position = 0,
            ValueFromPipeline,
            ValueFromPipelineByPropertyName,
            ParameterSetName = "Items"
        )]
        [string[]] $Path = ".",

        [Parameter(Position = 1)]
        [string] $Filter,

        [Parameter(ValueFromPipelineByPropertyName)]
        [Alias("PSPath", "LP")]
        [string[]] $LiteralPath,

        [string[]] $Include,

        [string[]] $Exclude,

        [Alias("s")]
        [switch] $Recurse = $false,

        [uint] $Depth,

        [switch] $Force = $false,

        [switch] $Name,

        [System.Management.Automation.FlagsExpression[System.IO.FileAttributes]] $Attributes,

        # NOTE: Should be Dynamic
        [switch] $FollowSymlink,

        # NOTE: Should be Dynamic
        [Alias("ad")]
        [switch] $Directory,

        # NOTE: Should be Dynamic
        [Alias("af")]
        [switch] $File,

        # NOTE: Should be Dynamic
        [Alias("ah", "h")]
        [switch] $Hidden,

        # NOTE: Should be Dynamic
        [Alias("ar")]
        [switch] $ReadOnly,

        # NOTE: Should be Dynamic
        [Alias("as")]
        [switch] $System
    )

    if ($BinaryLength) {
        $LengthSelector = $BinarySelector
    } else {
        if ($ByteLength) {
            $LengthSelector = $NumberSelector
        } else {
            $LengthSelector = $DecimalSelector
        }
    }

    $Expressions = ("Get-ChildItem")
    if ($Path) { $Expressions += " -Path '" + ($Path -join "','") + "'" }
    if ($Filter) { $Expressions += " -Filter $Filter" }
    if ($LiteralPath) { $Expressions += " -LiteralPath '" + ($LiteralPath -join "','") + "'" }
    if ($Include) { $Expressions += " -Include '" + ($Include -join "','") + "'" }
    if ($Exclude) { $Expressions += " -Exclude '" + ($Exclude -join "','") + "'" }
    if ($Recurse) { $Expressions += " -Recurse" }
    if ($Depth) { $Expressions += " -Depth $Depth" }
    if ($Force) { $Expressions += " -Force" }
    if ($Name) { $Expressions += " -Name" }
    if ($Attributes) { $Expressions += " -Attributes $Attributes" }
    if ($FollowSymlink) { $Expressions += " -FollowSymlink" }
    if ($Directory) { $Expressions += " -Directory" }
    if ($File) { $Expressions += " -File" }
    if ($Hidden) { $Expressions += " -Hidden" }
    if ($ReadOnly) { $Expressions += " -ReadOnly" }
    if ($System) { $Expressions += " -System" }

    Invoke-Expression ($Expressions -join " ") | foreach {
        if ("System.IO.FileSystemInfo" -ne $_.GetType().BaseType) {

            # System.ValueType or System.MarshalByRefObject
            $_

        } else {

            # Mode
            $ModeVt = "$($_.Mode -replace '-', '−')  " # Ligatures: replace Hyphen-Minus with Minus Sign

            # LastWriteTime
            $LastWriteTime = $_.LastWriteTime
            $FromNow = (Get-Date) - $LastWriteTime
            $Index = 0
            while ($FromNow -gt $DateSelector.Terms[$Index]) { $Index++ }
            $LastWriteTimeVt = "$(Fg8 $DateSelector.Codes[$Index])$($LastWriteTime.ToString($DateStyle))  $($LastWriteTime.ToString($TimeStyle))$(Fg)  "

            # Length
            $IsLink = "SymbolicLink" -eq $_.LinkType
            if ((Test-Path $_ -PathType Leaf) -and -not $IsLink) {
                $Length = [double]$_.Length
                $Index = 0
                while ($Length -ge $LengthSelector.Terms[$Index]) { $Index++ }
                if ($Index -gt 0) {
                    $Length = $Length / $LengthSelector.Terms[$Index - 1]
                }
                $LengthVt = "$(Fg8 $LengthSelector.Codes[$Index])$($LengthSelector.Format -f $length) $($LengthSelector.Units[$Index])$(Fg)  "
            } else {
                $LengthVt = ""
            }

            # Name
            $Key = $_.Name
            if ($IconsByName.ContainsKey($Key)) {
                $Icon = $IconsByName[$Key]
            } else {
                $Key = $_.Extension
                if ($IconsByExtension.ContainsKey($Key)) {
                    $Icon = $IconsByExtension[$Key]
                } else {
                    $Key = $_.GetType().Name
                    if ($IconsByType.ContainsKey($Key)) {
                        $Icon = $IconsByType[$Key]
                    } else {
                        $Icon = " "
                    }
                }
            }
            if ($_.Target) {
                if ([IO.Path]::IsPathFullyQualified($_.Target)) {
                    $Target = $_.Target
                } else {
                    if ($_.DirectoryName) {
                        $Target = Join-Path $_.DirectoryName $_.Target
                    } else {
                        if ($_.Parent) {
                            $Target = Join-Path $_.Parent $_.Target
                        } else {
                            $Target = $_.Target
                        }
                    }
                }

                if (Test-Path $Target -PathType Container) {
                    $Format = "$(Fg8 $ItemCodes.Link){0} {1}$(Fg) —→ $(fg8 $ItemCodes.Directory){2}$(Fg)"
                } else {
                    if (Test-Path $Target -PathType Leaf) {
                        $Format = "$(Fg8 $ItemCodes.Link){0} {1}$(Fg) —→ {2}"
                    } else {
                        $Format = "$(Fg8 $ItemCodes.Link){0} {1}$(Fg) —→ $(Fg8 $ItemCodes.BrokenLink){2}$(Fg)"
                    }
                }
            } else {
                if (Test-Path $_ -PathType Container) {
                    $Format = "$(Fg8 $ItemCodes.Directory){0} {1}$(Fg)"
                } else {
                    $Format = "{0} {1}"
                }
            }
            $NameVt = $Format -f $Icon, $_.Name, $_.Target

            # NOTE: Add member one by one to ensure correct ordering
            $Item = New-Object PSCustomObject
            $Item |
                Add-Member Mode $ModeVt -PassThru |
                Add-Member LastWriteTime $LastWriteTimeVt -PassThru |
                Add-Member Length $LengthVt -PassThru |
                Add-Member Name $NameVt
            $Item
        }
    }
}

Export-ModuleMember -Function Get-ChildItemVt
